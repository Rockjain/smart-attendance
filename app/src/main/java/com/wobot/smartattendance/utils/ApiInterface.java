package com.wobot.smartattendance.utils;

import com.wobot.smartattendance.modals.FaceEnrollResponse;
import com.wobot.smartattendance.modals.LocationResponse;
import com.wobot.smartattendance.modals.MarkResponse;
import com.wobot.smartattendance.modals.NewDeviceRegisterResponse;
import com.wobot.smartattendance.modals.OtpCheckResponse;
import com.wobot.smartattendance.modals.RegisterUserResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("splashCheck")
    public Call<LocationResponse> checkLocation(@Field("lat") String lat, @Field("long") String longt, @Field("imei") String imei);

    @FormUrlEncoded
    @POST("registerUser")
    public Call<RegisterUserResponse> registerUser(@Field("emp_id") String empid, @Field("name") String name, @Field("phone_no") String phone, @Field("imei") String imei);

    @FormUrlEncoded
    @POST("registerNewDevice")
    public Call<NewDeviceRegisterResponse> registerNewDevice(@Field("emp_id") String empid, @Field("name") String name, @Field("phone_no") String phone, @Field("imei") String imei);

    @FormUrlEncoded
    @POST("confirmOtp")
    public Call<OtpCheckResponse> checkOtp(@Field("emp_id") String emp_id, @Field("user_id") String user_id, @Field("otp") String otp);

    @FormUrlEncoded
    @POST("faceEnroll")
    public Call<FaceEnrollResponse> enrollFace(@Field("emp_id") String emp_id, @Field("user_id") String user_id, @Field("source_image") String source_image);

    @FormUrlEncoded
    @POST("markAttendence")
    public Call<MarkResponse> markAttendence(@Field("emp_id") String emp_id, @Field("user_id") String user_id, @Field("type") String type);

}
