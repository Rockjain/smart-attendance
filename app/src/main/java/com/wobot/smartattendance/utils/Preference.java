package com.wobot.smartattendance.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Preference {

    public static String getString(Context ctx, String key) {
        String string = "";
        SharedPreferences preferences = ctx.getSharedPreferences(Constants.PREFERENCE,Context.MODE_PRIVATE);
        string = preferences.getString(key, "");
        return string;
    }

    public static void setString(Context ctx, String key, String value) {
        SharedPreferences preferences =  ctx.getSharedPreferences(Constants.PREFERENCE,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.commit();
    }
}
