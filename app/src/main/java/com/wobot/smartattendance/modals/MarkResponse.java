package com.wobot.smartattendance.modals;

public class MarkResponse {
    private String message;
    private String user_id;
    private String emp_id;
    private String is_mark;
    private int error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getIs_mark() {
        return is_mark;
    }

    public void setIs_mark(String is_mark) {
        this.is_mark = is_mark;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }
}
