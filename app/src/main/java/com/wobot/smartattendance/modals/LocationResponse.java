package com.wobot.smartattendance.modals;

public class LocationResponse {
    private int in_zone;
    private String message;
    private int is_login;
    private int is_register;
    private int is_device_registered;
    private int is_verified;
    private int is_face_enroll;
    private int today_status;
    private String is_mark;
    private String user_id;
    private String emp_id;
    private String name;
    private String source_image_url;
    private String source_image;

    public int getIn_zone() {
        return in_zone;
    }

    public void setIn_zone(int in_zone) {
        this.in_zone = in_zone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getIs_login() {
        return is_login;
    }

    public void setIs_login(int is_login) {
        this.is_login = is_login;
    }

    public int getIs_register() {
        return is_register;
    }

    public void setIs_register(int is_register) {
        this.is_register = is_register;
    }

    public int getIs_device_registered() {
        return is_device_registered;
    }

    public void setIs_device_registered(int is_device_registered) {
        this.is_device_registered = is_device_registered;
    }

    public int getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(int is_verified) {
        this.is_verified = is_verified;
    }

    public int getIs_face_enroll() {
        return is_face_enroll;
    }

    public void setIs_face_enroll(int is_face_enroll) {
        this.is_face_enroll = is_face_enroll;
    }

    public String getIs_mark() {
        return is_mark;
    }

    public void setIs_mark(String is_mark) {
        this.is_mark = is_mark;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource_image_url() {
        return source_image_url;
    }

    public void setSource_image_url(String source_image_url) {
        this.source_image_url = source_image_url;
    }

    public String getSource_image() {
        return source_image;
    }

    public void setSource_image(String source_image) {
        this.source_image = source_image;
    }

    public int getToday_status() {
        return today_status;
    }

    public void setToday_status(int today_status) {
        this.today_status = today_status;
    }
}
