package com.wobot.smartattendance.modals;

public class FaceEnrollResponse {
    private String message;
    private String user_id;
    private String emp_id;
    private int is_login;
    private int is_register;
    private int is_verified;
    private int is_face_enroll;
    private int is_device_registered;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public int getIs_login() {
        return is_login;
    }

    public void setIs_login(int is_login) {
        this.is_login = is_login;
    }

    public int getIs_register() {
        return is_register;
    }

    public void setIs_register(int is_register) {
        this.is_register = is_register;
    }

    public int getIs_verified() {
        return is_verified;
    }

    public void setIs_verified(int is_verified) {
        this.is_verified = is_verified;
    }

    public int getIs_face_enroll() {
        return is_face_enroll;
    }

    public void setIs_face_enroll(int is_face_enroll) {
        this.is_face_enroll = is_face_enroll;
    }

    public int getIs_device_registered() {
        return is_device_registered;
    }

    public void setIs_device_registered(int is_device_registered) {
        this.is_device_registered = is_device_registered;
    }
}
