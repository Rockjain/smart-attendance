package com.wobot.smartattendance.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.wobot.smartattendance.R;
import com.wobot.smartattendance.modals.LocationResponse;
import com.wobot.smartattendance.utils.ApiClient;
import com.wobot.smartattendance.utils.ApiInterface;
import com.wobot.smartattendance.utils.Constants;
import com.wobot.smartattendance.utils.Preference;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Splash extends AppCompatActivity implements LocationListener{
    final String TAG = "Splash Activity";
    private final int SPLASH_DISPLAY_TIME = 1000;
    private static final int MY_REQUEST_PERMISSION = 99;
    private static final int MY_REQUEST_LOCATION_PERMISSION = 101;
    private static final int MY_REQUEST_PHONESTATE_PERMISSION = 100;
    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    // How many Geocoder should return our GPSTracker
    int geocoderMaxResults = 1;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    String IMEI;
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 20; // 20 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 5000; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;
    ApiInterface apiInterface;
    ProgressBar progressBar;
    FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Fabric.with(Splash.this,new Crashlytics());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        progressBar=findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        checkPermission();
    }

    public void checkPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.CAMERA)
                    + ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.READ_PHONE_STATE)
                    + ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Splash.this,new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE,  Manifest.permission.ACCESS_FINE_LOCATION}, MY_REQUEST_PERMISSION);
            } else if (ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.CAMERA)
                    + ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.READ_PHONE_STATE)
                    + ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                getDevieceID();
                getLocation();
            }
        } else {
            getDevieceID();
            getLocation();
        }
    }

    public void getDevieceID() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Splash.this,new String[]{Manifest.permission.READ_PHONE_STATE},MY_REQUEST_PHONESTATE_PERMISSION);
            }
        }
        Log.d("IMEI Number : ",telephonyManager.getDeviceId());
        IMEI=telephonyManager.getDeviceId();
        Crashlytics.setUserIdentifier(telephonyManager.getDeviceId());
        mFirebaseAnalytics.setUserId(telephonyManager.getDeviceId());
        mFirebaseAnalytics.setUserProperty("IMEI",telephonyManager.getDeviceId());
        Preference.setString(Splash.this,Constants.IMEI_NUMBER,telephonyManager.getDeviceId());
    }
    public void getLocation() {
        try {
            locationManager = (LocationManager) Splash.this
                    .getSystemService(LOCATION_SERVICE);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (!isNetworkEnabled && !isGPSEnabled) {
                // no network provider is enabled
                android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                builder.setMessage("To continue, turn on device location on your phone.")
                        .setCancelable(false)
                        .setNegativeButton("No,Thanks", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                android.os.Process.killProcess(android.os.Process.myPid());
                            }
                        })
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                            }
                        }).show();
            } else {
                this.canGetLocation = true;

                // First get location from Network Provider
                if (isNetworkEnabled) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                                + ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},MY_REQUEST_LOCATION_PERMISSION);
                        }
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d(TAG, "Network enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if(location!=null) {
                            updateGPSCoordinates();
                            checkLocation();
                        }
                    }

                }

                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d(TAG,"GPS is enabled");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if(location!=null) {
                            updateGPSCoordinates();
                            checkLocation();
                        }
                    }
                 }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkLocation(){
        if (IMEI.equals("")){
            getDevieceID();
        }
        else if (latitude == 0.0 || longitude == 0.0){
            Log.d(TAG,"Latitude : 0.0 Longitude : 0.0");
            getLocation();
        }
        else {
            progressBar.setVisibility(View.VISIBLE);
            Call<LocationResponse> locationResponseCall= (Call<LocationResponse>) apiInterface.checkLocation(String.valueOf(latitude),String.valueOf(longitude),IMEI);
            locationResponseCall.enqueue(new Callback<LocationResponse>() {
                @Override
                public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    Log.d("In_zone  0 : ", String.valueOf(response.body().getIn_zone()));
                    Log.d("Is_register 0 : ", String.valueOf(response.body().getIs_register()));
                    Log.d("Is_device_register 0 : ", String.valueOf(response.body().getIs_device_registered()));
                    Log.d("Is_verified 0 : ", String.valueOf(response.body().getIs_verified()));
                    Log.d("Is_face_enroll 0 : ", String.valueOf(response.body().getIs_face_enroll()));
                    Log.d("Is_login 0 : ", String.valueOf(response.body().getIs_login()));
                    if (response.body().getIn_zone() == 0){
                        Log.d("In_zone  0 : ", String.valueOf(response.body().getIn_zone()));
                        android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                        builder.setMessage(response.body().getMessage())
                                .setCancelable(false)
                                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        android.os.Process.killProcess(android.os.Process.myPid());
                                    }
                                }).show();
                    }else if (response.body().getIs_register() == 0){
                        Log.d("Is_register 0 : ", String.valueOf(response.body().getIs_register()));
                        Toast.makeText(Splash.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(Splash.this, Register.class);
                        startActivity(intent);
                        finish();
                    }else if (response.body().getIs_device_registered() == 0){
                        Log.d("Is_device_register 0 : ", String.valueOf(response.body().getIs_device_registered()));
                        Toast.makeText(Splash.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(Splash.this, Register.class);
                        startActivity(intent);
                        finish();
                    }else if (response.body().getIs_verified() == 0){
                        Log.d("Is_verified 0 : ", String.valueOf(response.body().getIs_verified()));
                        Toast.makeText(Splash.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(Splash.this, Register.class);
                        startActivity(intent);
                        finish();
                    }else if (response.body().getIs_face_enroll() == 0){
                        Log.d("Is_face_enroll 0 : ", String.valueOf(response.body().getIs_face_enroll()));
                        Toast.makeText(Splash.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(Splash.this,MessageActivity.class);
                        startActivity(intent);
                        finish();
                    }else if (response.body().getIs_login() == 0){
                        Log.d("Is_login 0 : ", String.valueOf(response.body().getIs_login()));
                        Toast.makeText(Splash.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(Splash.this, Register.class);
                        startActivity(intent);
                        finish();
                    }else {
                        Log.d("Suceessfully login : ",response.body().getMessage());
                        Preference.setString(Splash.this,Constants.EMP_ID,response.body().getEmp_id());
                        Preference.setString(Splash.this,Constants.USER_ID,response.body().getUser_id());
                        Preference.setString(Splash.this,Constants.TODAYMARK,response.body().getIs_mark());
                        Preference.setString(Splash.this,Constants.IMAGENAME,response.body().getSource_image());
                        Preference.setString(Splash.this,Constants.TODAYSTATUS, String.valueOf(response.body().getToday_status()));
                        Intent intent=new Intent(Splash.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<LocationResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Bundle bundle = new Bundle();
                    bundle.putString("check_location_error",t.toString());
                    mFirebaseAnalytics.logEvent("splash_activity",bundle);
                    Log.e(TAG,"Check Location error : "+t.toString());
                    android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                    builder.setMessage("Something went wrong.")
                            .setCancelable(false)
                            .setPositiveButton("Try again?", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                   checkLocation();
                                }
                            }).show();
                }
            });
        }
    }

    public void updateGPSCoordinates() {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Bundle bundle = new Bundle();
            bundle.putString("Latitude", String.valueOf(latitude));
            bundle.putString("Longitude", String.valueOf(longitude));
            mFirebaseAnalytics.logEvent("Location",bundle);
            Log.d(TAG,"Latitue : "+latitude+" Longitude : "+longitude);
        }
    }

    @Override
    public void onLocationChanged(Location loc) {
         location=loc;
         updateGPSCoordinates();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_REQUEST_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getDevieceID();
                    getLocation();
                } else {
                  Log.d(TAG,"Permission denied");
                }
                return;
            }
            case MY_REQUEST_PHONESTATE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getDevieceID();
                } else {
                    Log.d(TAG,"Permission denied");
                }
                return;
            }
            case MY_REQUEST_LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    Log.d(TAG,"Permission denied");
                }
                return;
            }

        }

    }

}
