package com.wobot.smartattendance.Activities;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.wobot.smartattendance.R;
import com.wobot.smartattendance.modals.OtpCheckResponse;
import com.wobot.smartattendance.modals.RegisterUserResponse;
import com.wobot.smartattendance.utils.ApiClient;
import com.wobot.smartattendance.utils.ApiInterface;
import com.wobot.smartattendance.utils.Constants;
import com.wobot.smartattendance.utils.Preference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Otp extends AppCompatActivity {
    final String TAG="Otp Activity";
    EditText etotp;
    TextView tvotp;
    Button blogin;
    RegisterUserResponse registerUserResponse;
    ApiInterface apiInterface;
    ProgressBar progressBar;
    String empid, userid;
    FirebaseAnalytics firebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        firebaseAnalytics=FirebaseAnalytics.getInstance(this);
        progressBar=findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        apiInterface= ApiClient.getClient().create(ApiInterface.class);

        tvotp=findViewById(R.id.tv_OTP);
        etotp=findViewById(R.id.et_otp);
        blogin=findViewById(R.id.b_login);

       Log.d(TAG, Preference.getString(Otp.this, Constants.OTP));
       tvotp.setText(Preference.getString(Otp.this, Constants.OTP));
        empid=Preference.getString(Otp.this, Constants.EMP_ID);
        userid=Preference.getString(Otp.this, Constants.USER_ID);
        Bundle bundle = new Bundle();
        bundle.putString("emp_id", empid);
        bundle.putString("user_id", userid);
        firebaseAnalytics.logEvent("otp_screen",bundle);
        blogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             if (etotp.getText().equals("")){
                 Toast.makeText(Otp.this,"Please enter display otp",Toast.LENGTH_SHORT).show();
             }else {
                 Log.d("otp emp id : ",empid);
                 Log.d("otp user id : ",userid);
                 Log.d("otp otp : ",etotp.getText().toString());
                 progressBar.setVisibility(View.VISIBLE);
                 Call<OtpCheckResponse> otpCheckResponseCall= apiInterface.checkOtp(empid,userid,etotp.getText().toString());
                 otpCheckResponseCall.enqueue(new Callback<OtpCheckResponse>() {
                     @Override
                     public void onResponse(Call<OtpCheckResponse> call, Response<OtpCheckResponse> response) {
                         progressBar.setVisibility(View.GONE);

                         if (response.body().getIs_verified() == 0){
                             Log.d("otp response : ",response.body().getMessage());
                             Toast.makeText(Otp.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                         }else if (response.body().getIs_face_enroll() == 0){
                             Log.d("otp response : ",response.body().getMessage());
                             Toast.makeText(Otp.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                             Intent intent=new Intent(Otp.this,MessageActivity.class);
                             startActivity(intent);
                             finish();
                         }
                     }

                     @Override
                     public void onFailure(Call<OtpCheckResponse> call, Throwable t) {
                         progressBar.setVisibility(View.GONE);
                         Log.e(TAG,"OTP check error"+t.toString());
                         Bundle bundle = new Bundle();
                         bundle.putString("otp_check_erro", t.toString());
                         firebaseAnalytics.logEvent("otp_screen",bundle);
                     }
                 });
             }
            }
        });
    }
}
