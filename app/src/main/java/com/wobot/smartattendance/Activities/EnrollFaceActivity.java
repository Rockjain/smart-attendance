package com.wobot.smartattendance.Activities;

import android.content.DialogInterface;
import android.nfc.Tag;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.CountDownTimer;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.wobot.smartattendance.R;
import com.wobot.smartattendance.modals.FaceEnrollResponse;
import com.wobot.smartattendance.utils.ApiClient;
import com.wobot.smartattendance.utils.ApiInterface;
import com.wobot.smartattendance.utils.Constants;
import com.wobot.smartattendance.utils.Preference;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnrollFaceActivity extends AppCompatActivity {
    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private ImageButton capture;
    private static Context myContext;
    private LinearLayout cameraPreview;
    private boolean cameraFront = true;
    public static Bitmap bitmap;
    final String TAG="Enroll Face Activity";
    String empid, userid;
    ApiInterface apiInterface;
    ProgressBar progressBar;
    FirebaseAnalytics firebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enroll_face);
        firebaseAnalytics=FirebaseAnalytics.getInstance(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;
        mCamera = Camera.open(1);
        mCamera.setDisplayOrientation(90);
        mPicture = getPictureCallback();
        cameraPreview = (LinearLayout) findViewById(R.id.cPreview);
        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);

        progressBar=findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        empid= Preference.getString(EnrollFaceActivity.this, Constants.EMP_ID);
        userid=Preference.getString(EnrollFaceActivity.this, Constants.USER_ID);
        Bundle bundle = new Bundle();
        bundle.putString("emp_id", empid);
        bundle.putString("user_id", userid);
        firebaseAnalytics.logEvent("enroll_face_screen",bundle);
        capture = (ImageButton) findViewById(R.id.btnCam);
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.takePicture(null, null, mPicture);
            }
        });
        mCamera.startPreview();
    }

    private Camera.PictureCallback getPictureCallback() {
        Log.d(TAG,"Inside getPictureCallback ");
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                progressBar.setVisibility(View.VISIBLE);
                capture.setVisibility(View.GONE);
                String encodedImage = Base64.encodeToString(data, Base64.DEFAULT);
                Log.d(TAG,encodedImage);
                faceEnroll(encodedImage);
            }
        };
        return picture;
    }

    public void faceEnroll( String image){
        Call<FaceEnrollResponse> faceEnrollResponseCall=apiInterface.enrollFace(empid,userid,image);
        faceEnrollResponseCall.enqueue(new Callback<FaceEnrollResponse>() {
            @Override
            public void onResponse(Call<FaceEnrollResponse> call, Response<FaceEnrollResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body().getIs_face_enroll() == 0){
                    Log.d("Enroll face response : ",response.body().getMessage());
                    Toast.makeText(EnrollFaceActivity.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }else if (response.body().getIs_face_enroll() == 1){
                    Log.d("Enroll face response : ",response.body().getMessage());

                    AlertDialog.Builder builder=new AlertDialog.Builder(EnrollFaceActivity.this);
                    builder.setMessage(response.body().getMessage())
                            .setCancelable(false)
                            .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Intent.ACTION_MAIN);
                                    intent.addCategory(Intent.CATEGORY_HOME);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    android.os.Process.killProcess(android.os.Process.myPid());
                                }
                            }).show();

                }
            }

            @Override
            public void onFailure(Call<FaceEnrollResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                capture.setVisibility(View.VISIBLE);
                Log.e(TAG,"Face enroll error"+t.toString());
                Bundle bundle = new Bundle();
                bundle.putString("face_enroll_error", t.toString());
                firebaseAnalytics.logEvent("enroll_face_screen",bundle);
            }
        });
    }
}
