package com.wobot.smartattendance.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClient;
import com.amazonaws.services.rekognition.model.CompareFacesMatch;
import com.amazonaws.services.rekognition.model.CompareFacesRequest;
import com.amazonaws.services.rekognition.model.CompareFacesResult;
import com.amazonaws.services.rekognition.model.ComparedFace;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.S3Object;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.wobot.smartattendance.R;
import com.wobot.smartattendance.modals.MarkResponse;
import com.wobot.smartattendance.utils.ApiClient;
import com.wobot.smartattendance.utils.ApiInterface;
import com.wobot.smartattendance.utils.Constants;
import com.wobot.smartattendance.utils.Preference;

import java.nio.ByteBuffer;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarkActivity extends AppCompatActivity {
    private Camera mCamera;
    private CameraPreview mPreview;
    private Camera.PictureCallback mPicture;
    private Button capture, switchCamera;
    private Context myContext;
    private LinearLayout cameraPreview;
    private boolean cameraFront = true;
    public static Bitmap bitmap;
    String empid, userid, imagename;
    ApiInterface apiInterface;
    ProgressBar progressBar;
    Float similarityThreshold = 80F;
    Image targetimage, sourceimage;
    String sourcestring, markstatus,marktype;
    AmazonRekognition amazonRekognition;
    final String TAG="Mark Activity";
    ProgressDialog progressDialog;
    int attempt=1;
    FirebaseAnalytics firebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        myContext = this;
        firebaseAnalytics=FirebaseAnalytics.getInstance(this);
        mCamera =  Camera.open(1);
        mCamera.setDisplayOrientation(90);
        mPicture=getPictureCallback();
        cameraPreview = (LinearLayout) findViewById(R.id.cPreview);
        mPreview = new CameraPreview(myContext, mCamera);
        cameraPreview.addView(mPreview);

        progressBar=findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        apiInterface= ApiClient.getClient().create(ApiInterface.class);

        empid= Preference.getString(MarkActivity.this, Constants.EMP_ID);
        userid=Preference.getString(MarkActivity.this, Constants.USER_ID);
        imagename=Preference.getString(MarkActivity.this, Constants.IMAGENAME);
        markstatus=Preference.getString(MarkActivity.this, Constants.TODAYMARK);
        Bundle bundle = new Bundle();
        bundle.putString("emp_id", empid);
        bundle.putString("user_id", userid);
        bundle.putString("image_name", imagename);
        bundle.putString("mark_status", markstatus);
        firebaseAnalytics.logEvent("mark_face_screen",bundle);
        if (markstatus.equals("OUT")){
            Log.e(TAG,"Inside out");
            marktype="1";
        }else {
            Log.e(TAG,"Inside in");
            marktype="0";
        }
        Log.e(TAG,"Mark Status : "+markstatus);
        Log.e(TAG,"Mark Type : "+marktype);
        amazonRekognition=new AmazonRekognitionClient(new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return Constants.accessKey;
            }

            @Override
            public String getAWSSecretKey() {
                return Constants.secretKey;
            }
        });
        amazonRekognition.setRegion(Region.getRegion("ap-south-1"));

        mCamera.startPreview();

        new CountDownTimer(5000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                mCamera.takePicture(null, null, mPicture);
            }
        }.start();

    }


    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
               // Preference.setString(MarkActivity.this,Constants.TARGET_IMAGE,data.toString());
                   new CompareFace(MarkActivity.this).execute();

               targetimage = new Image()
                      .withBytes(ByteBuffer.wrap(data));
                //compareFace();
            }
        };
        return picture;
    }


    private void releaseCamera() {
        // stop and release camera
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private void startCamera(){
        mCamera = Camera.open(1);
        mCamera.setDisplayOrientation(90);
        mPicture = getPictureCallback();
        mPreview.refreshCamera(mCamera);
    }

    private void startTimer(){
        new CountDownTimer(5000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                mCamera.takePicture(null, null, mPicture);
            }
        }.start();
    }

    public class CompareFace extends AsyncTask<Void,Void, CompareFacesResult> {
        Context context;
        public CompareFace(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           progressDialog=new ProgressDialog(MarkActivity.this);
           progressDialog.setMessage("Please wait...");
           progressDialog.setCancelable(false);
           progressDialog.show();
        }

        @Override
        protected CompareFacesResult doInBackground(Void... voids) {
            try  {
            CompareFacesRequest request = new CompareFacesRequest()
                    .withSourceImage(new Image().withS3Object(new S3Object().withBucket("wobotattendance").withName(imagename)))
                    .withTargetImage(targetimage).withSimilarityThreshold(similarityThreshold);

            CompareFacesResult compareFacesResult=amazonRekognition.compareFaces(request);
            return compareFacesResult;
            } catch (Exception e) {
                Log.e(TAG,e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(CompareFacesResult compareFacesResult) {
            super.onPostExecute(compareFacesResult);
           progressDialog.dismiss();
           Log.d(TAG,"compareFacesResult : "+compareFacesResult);
            if (compareFacesResult != null) {
                List<CompareFacesMatch> faceDetails = compareFacesResult.getFaceMatches();
                List<ComparedFace> uncompared = compareFacesResult.getUnmatchedFaces();
                if (uncompared.size() > 0) {
                    Log.d(TAG, "There was " + uncompared.size()
                            + " face(s) that did not match");
                    if (attempt <= 3) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MarkActivity.this);
                        builder.setMessage("Attendance cannot be captured\n"+
                                "as the face does not match\n" +
                                "with the enroled employee.\n"+
                                "Request you to try again.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        releaseCamera();
                                        startCamera();
                                        startTimer();
                                    }
                                });
                        AlertDialog dialog = builder.show();
                        TextView messageText = dialog.findViewById(android.R.id.message);
                        messageText.setGravity(Gravity.CENTER);
                        attempt++;
                    }else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MarkActivity.this);
                        builder.setMessage("Sorry, attendance cannot be captured. You can now exit the App.")
                                .setCancelable(false)
                                .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        android.os.Process.killProcess(android.os.Process.myPid());
                                    }
                                }).show();
                    }

                } else {
                    for (CompareFacesMatch match : faceDetails) {
                        ComparedFace face = match.getFace();
                        Log.d(TAG, "Face matches with " + face.getConfidence().toString()
                                + "% confidence " + match.getSimilarity().toString() + "% Similarity");
                         markAttendance();
                    }
                }
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(MarkActivity.this);
                builder.setMessage("Face parameter does not match, Please try again.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                releaseCamera();
                                startCamera();
                                startTimer();
                            }
                        }).show();
            }
        }
    }

    public  void markAttendance(){
        progressBar.setVisibility(View.VISIBLE);
        Call<MarkResponse> markResponseCall=apiInterface.markAttendence(empid,userid,marktype);
        markResponseCall.enqueue(new Callback<MarkResponse>() {
            @Override
            public void onResponse(Call<MarkResponse> call, Response<MarkResponse> response) {
                progressBar.setVisibility(View.GONE);
                Log.d(TAG,response.body().getMessage());
                if (response.body().getError()==1){
                    Toast.makeText(MarkActivity.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }else{

                    AlertDialog.Builder builder=new AlertDialog.Builder(MarkActivity.this);
                    builder.setMessage(response.body().getMessage())
                            .setCancelable(false)
                            .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Intent.ACTION_MAIN);
                                    intent.addCategory(Intent.CATEGORY_HOME);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    android.os.Process.killProcess(android.os.Process.myPid());
                                }
                            }).show();
                }

            }

            @Override
            public void onFailure(Call<MarkResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.d(TAG,"Mark attendance error : "+t.toString());
                Bundle bundle = new Bundle();
                bundle.putString("mark_attendance_error", t.toString());
                firebaseAnalytics.logEvent("mark_face_screen",bundle);
            }
        });
    }

}
