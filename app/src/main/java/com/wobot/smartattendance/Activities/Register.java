package com.wobot.smartattendance.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.wobot.smartattendance.R;
import com.wobot.smartattendance.modals.NewDeviceRegisterResponse;
import com.wobot.smartattendance.modals.RegisterUserResponse;
import com.wobot.smartattendance.utils.ApiClient;
import com.wobot.smartattendance.utils.ApiInterface;
import com.wobot.smartattendance.utils.Constants;
import com.wobot.smartattendance.utils.Preference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity implements View.OnClickListener {
EditText etempid, etname, etmobileno;
Button bnext;
    ApiInterface apiInterface;
    ProgressBar progressBar;
    String IMEI;
    final String TAG="Register Activity";
    private static final int MY_REQUEST_PHONESTATE_PERMISSION = 100;
    FirebaseAnalytics firebaseAnalytics;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        firebaseAnalytics=FirebaseAnalytics.getInstance(this);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        progressBar=findViewById(R.id.progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        apiInterface= ApiClient.getClient().create(ApiInterface.class);

        etempid= findViewById(R.id.et_emp_id);
        etname= findViewById(R.id.et_emp_name);
        etmobileno= findViewById(R.id.et_mobile_no);
        bnext= findViewById(R.id.b_next);
        IMEI= Preference.getString(Register.this, Constants.IMEI_NUMBER);
        Bundle bundle = new Bundle();
        bundle.putString("IMEI", IMEI);
        firebaseAnalytics.logEvent("register_screen",bundle);
        bnext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.b_next:
                if (etempid.getText().equals("")){
                    Toast.makeText(Register.this,"Please enter Employee Id",Toast.LENGTH_SHORT).show();
                }else if (etname.getText().equals("")){
                    Toast.makeText(Register.this,"Please enter name",Toast.LENGTH_SHORT).show();
                }else if (etmobileno.getText().equals("")){
                    Toast.makeText(Register.this,"Please enter mobile number",Toast.LENGTH_SHORT).show();
                }else if (IMEI.equals("")|| IMEI.equals(null)){
                    getDevieceID();
                }else {
                    progressBar.setVisibility(View.VISIBLE);
                    Call<RegisterUserResponse> registerUserResponseCall= (Call<RegisterUserResponse>) apiInterface.registerUser(etempid.getText().toString(),etname.getText().toString(),etmobileno.getText().toString(),IMEI);
                    registerUserResponseCall.enqueue(new Callback<RegisterUserResponse>() {
                        @Override
                        public void onResponse(Call<RegisterUserResponse> call, Response<RegisterUserResponse> response) {
                            progressBar.setVisibility(View.GONE);
                            if (response.body().getIs_register() == 0) {
                                Toast.makeText(Register.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }else if (response.body().getIs_verified() == 0) {
                                Preference.setString(Register.this,Constants.OTP,response.body().getOtp());
                                Preference.setString(Register.this,Constants.EMP_ID,response.body().getEmp_id());
                                Preference.setString(Register.this,Constants.USER_ID, String.valueOf(response.body().getUser_id()));
                                AlertDialog.Builder builder=new AlertDialog.Builder(Register.this);
                                builder.setMessage(response.body().getMessage())
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent=new Intent(Register.this,Otp.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }).show();

                            }else if (response.body().getIs_face_enroll() == 0) {
                                Preference.setString(Register.this,Constants.EMP_ID,response.body().getEmp_id());
                                Preference.setString(Register.this,Constants.USER_ID, String.valueOf(response.body().getUser_id()));
                                Toast.makeText(Register.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(Register.this,MessageActivity.class);
                                startActivity(intent);
                                finish();
                            }else if (response.body().getIs_device_registered() == 0){

                                android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
                                builder.setMessage(response.body().getMessage())
                                        .setCancelable(false)
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                                intent.addCategory(Intent.CATEGORY_HOME);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                android.os.Process.killProcess(android.os.Process.myPid());
                                            }
                                        })
                                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                progressBar.setVisibility(View.VISIBLE);
                                                Call<NewDeviceRegisterResponse> newDeviceRegisterResponseCall=apiInterface.registerNewDevice(etempid.getText().toString(),etname.getText().toString(),etmobileno.getText().toString(),IMEI);
                                                newDeviceRegisterResponseCall.enqueue(new Callback<NewDeviceRegisterResponse>() {
                                                    @Override
                                                    public void onResponse(Call<NewDeviceRegisterResponse> call, Response<NewDeviceRegisterResponse> response) {
                                                        progressBar.setVisibility(View.GONE);
                                                        if (response.body().getError()== 0){
                                                            android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
                                                            builder.setMessage(response.body().getMessage())
                                                                    .setCancelable(false)
                                                                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialog, int which) {
                                                                            Intent intent = new Intent(Intent.ACTION_MAIN);
                                                                            intent.addCategory(Intent.CATEGORY_HOME);
                                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                            startActivity(intent);
                                                                            android.os.Process.killProcess(android.os.Process.myPid());
                                                                        }
                                                                    }).show();
                                                        }else {
                                                            Toast.makeText(Register.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(Call<NewDeviceRegisterResponse> call, Throwable t) {

                                                    }
                                                });
                                            }
                                        }).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<RegisterUserResponse> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            Log.e(TAG,"register user error"+t.toString());
                            Bundle bundle = new Bundle();
                            bundle.putString("register_user_error", t.toString());
                            firebaseAnalytics.logEvent("register_screen",bundle);
                        }
                    });
                }
                break;
        }
    }

    public void getDevieceID() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Register.this,new String[]{Manifest.permission.READ_PHONE_STATE},MY_REQUEST_PHONESTATE_PERMISSION);
            }
        }
        Log.d("IMEI Number : ",telephonyManager.getDeviceId());
        IMEI=telephonyManager.getDeviceId();
        Preference.setString(Register.this,Constants.IMEI_NUMBER,telephonyManager.getDeviceId());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case MY_REQUEST_PHONESTATE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getDevieceID();
                } else {
                    Log.d(TAG,"Permission denied");
                }
                return;
            }

        }

    }
}
