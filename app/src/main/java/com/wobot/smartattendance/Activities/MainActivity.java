package com.wobot.smartattendance.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClient;
import com.amazonaws.services.rekognition.model.Image;
import com.wobot.smartattendance.R;
import com.wobot.smartattendance.utils.ApiInterface;
import com.wobot.smartattendance.utils.Constants;
import com.wobot.smartattendance.utils.Preference;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView tvtime, tvdate, tvmessage;
    Button bmarkin, bmarkout;
    AmazonRekognitionClient rekognitionClient;
    AmazonRekognition amazonRekognition;
    String currentdate,currenttime,mark;
    SimpleDateFormat timeFormat= new SimpleDateFormat("hh:mm a");
    SimpleDateFormat dateFormat= new SimpleDateFormat("EEEE, dd MMM yyyy");
    ApiInterface apiInterface;
    ProgressBar progressBar;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    private static final int chooseCameraRequestCode = 100;
    Float similarityThreshold = 80F;
    final String TAG="Main Activity";
    Image targetimage, sourceimage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        currenttime=timeFormat.format(new Date());
        currentdate= dateFormat.format(new Date());

        tvtime=findViewById(R.id.tv_time);
        tvdate=findViewById(R.id.tv_date);
        bmarkin=findViewById(R.id.b_markin);
        bmarkout=findViewById(R.id.b_markout);

        tvtime.setText(currenttime);
        tvdate.setText(currentdate);
        if (Preference.getString(MainActivity.this,Constants.TODAYSTATUS).equals("1")) {
            if (Preference.getString(MainActivity.this, Constants.TODAYMARK).equals("OUT")) {
                bmarkin.setVisibility(View.VISIBLE);
                bmarkout.setVisibility(View.INVISIBLE);
            } else {
                bmarkout.setVisibility(View.VISIBLE);
                bmarkin.setVisibility(View.INVISIBLE);
            }
        }else {
            Log.d(TAG,Preference.getString(MainActivity.this,Constants.TODAYSTATUS));
            bmarkout.setVisibility(View.INVISIBLE);
            bmarkin.setVisibility(View.INVISIBLE);
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Attendance is Already Marked for today.")
                    .setCancelable(false)
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            android.os.Process.killProcess(android.os.Process.myPid());
                        }
                    }).show();
        }



        bmarkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           Intent intent=new Intent(MainActivity.this, MarkActivity.class);
           startActivity(intent);
           finish();
            }
        });

        bmarkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, MarkActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
