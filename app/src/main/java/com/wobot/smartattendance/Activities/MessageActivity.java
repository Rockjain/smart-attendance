package com.wobot.smartattendance.Activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.wobot.smartattendance.R;
import com.wobot.smartattendance.modals.FaceEnrollResponse;
import com.wobot.smartattendance.utils.ApiClient;
import com.wobot.smartattendance.utils.ApiInterface;
import com.wobot.smartattendance.utils.Constants;
import com.wobot.smartattendance.utils.Preference;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageActivity extends AppCompatActivity {
Button bcontinue;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    private static final int chooseCameraRequestCode = 100;
    final String TAG="Enroll Activity";
    String empid, userid;
    ApiInterface apiInterface;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

       bcontinue=findViewById(R.id.b_Msg_continue);
       bcontinue.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
             //openCamera();
               Intent intent=new Intent(MessageActivity.this,EnrollFaceActivity.class);
               startActivity(intent);
               finish();
           }
       });
    }


}
